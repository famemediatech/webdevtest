<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-23 12:55:53
         compiled from "C:\xampp\htdocs\ci\client\cp\default\famecms\modules\administrator\views\menu\category\index.html" */ ?>
<?php /*%%SmartyHeaderCode:23036576b79e94c3d01-27844702%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8dcc95776393c6b569d074ca54805b2013b10c02' => 
    array (
      0 => 'C:\\xampp\\htdocs\\ci\\client\\cp\\default\\famecms\\modules\\administrator\\views\\menu\\category\\index.html',
      1 => 1456634156,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '23036576b79e94c3d01-27844702',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_desc' => 0,
    'items' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_576b79e9501070_08449193',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_576b79e9501070_08449193')) {function content_576b79e9501070_08449193($_smarty_tpl) {?>
 <!-- Datatables Header -->
<div ng-controller="ManageCategoryCtrl">
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i><?php echo $_smarty_tpl->tpl_vars['page_desc']->value;?>

        </h1>
    </div>
</div>
<!--<ul class="breadcrumb breadcrumb-top">
    <li>Tables</li>
    <li><a href="">Datatables</a></li>
</ul>-->
<!-- END Datatables Header -->

<!-- Datatables Content -->
<div class="block full">
<div align="right"  class="block-title">
<div class="btn-group">
                    <a href="<?php echo base_url('administrator/category/add');?>
" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
 </div>
</div>
    <div class="table-responsive">
       <table id="example-datatable" class="table table-vcenter table-condensed">
            <thead>
                <tr>
                    <th class="text-center">Parent Name</th>
                    <th class="text-center">Category Name</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_smarty_tpl->tpl_vars['myId'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
 $_smarty_tpl->tpl_vars['myId']->value = $_smarty_tpl->tpl_vars['i']->key;
?>
                <tr>
                    <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['i']->value['parent_name'];?>
</td>
                    <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['i']->value['category_name'];?>
</td>
                    <td class="text-center"><span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['i']->value['status'];?>
</span></td>
                    <td class="text-center">
                        <div class="btn-group">
                        <a href="<?php echo base_url('administrator/category/edit');?>
/<?php echo $_smarty_tpl->tpl_vars['i']->value['id_category'];?>
" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                        <a ng-click="deletePermanentAction(<?php echo $_smarty_tpl->tpl_vars['i']->value['id_category'];?>
)" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>    
                    </div>
                    </td>
                </tr>
             <?php } ?>
            </tbody>
        </table>
		
    </div>
</div>
<!-- END Datatables Content -->
</div>

                   <?php }} ?>
