<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-24 01:20:55
         compiled from "C:\xampp\htdocs\ci\client\cp\default\famecms\modules\administrator\views\menu\setting\change-pass.html" */ ?>
<?php /*%%SmartyHeaderCode:19462576c25cde48758-62089374%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5e57ce3c8fb37860aebba980ff9f455d0dde5f9a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\ci\\client\\cp\\default\\famecms\\modules\\administrator\\views\\menu\\setting\\change-pass.html',
      1 => 1466705658,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19462576c25cde48758-62089374',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_576c25ce125265_91830279',
  'variables' => 
  array (
    'page_desc' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_576c25ce125265_91830279')) {function content_576c25ce125265_91830279($_smarty_tpl) {?>
<div ng-controller="ChangePassCtrl">
<div class="content-header">
                        <div class="header-section">
                            <h1>
                                <i class="fa fa-gear"></i><?php echo $_smarty_tpl->tpl_vars['page_desc']->value;?>

                            </h1>
                        </div>
 </div>
<!-- <ul class="breadcrumb breadcrumb-top">
    <li>Forms</li>
    <li><a href="">Components</a></li>
</ul>-->
<!-- END Components Header -->

<!-- Form Components Row -->
<div class="row">
    <div class="col-md-9">
        <!-- Select Components Block -->
        <div class="block">
            <!-- Select Components Content -->
             <form name="ChangePassForm" class="form-bordered" novalidate>
               <div class="form-group">
						<label for="Old-Password">Old Password</label>
						<input type="password" name="old_pass" class="form-control" ng-model="dataForm.old_pass" required>
                </div>
				<div class="form-group">
						<label for="new-password">New Password</label>
						<input type="text" name="new_password" class="form-control" ng-model="dataForm.new_pass" required>
                </div>
                
            <!-- END Select Components Content -->
        </div>
        <!-- END Select Components Block -->

       
    </div>
     <div class="col-md-3">
        <!-- Select Components Block -->
        <div class="block">
            <!-- Select Components Title -->
            <div class="block-title">
                <h2><strong>Manage</strong> </h2>
            </div>
            <!-- END Select Components Title -->

            <!-- Select Components Content -->
            <div class="form-group form-actions">
                    <button type="submit" ng-disabled="ChangePassForm.$invalid" ng-click="buttonUpdate(dataForm)" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Update</button>
            </div>
           
            <!-- END Select Components Content -->
        </div>
        <!-- END Select Components Block -->
       
    </div>

  
     </form>
</div>
<!-- END Form Components Row -->
</div><?php }} ?>
