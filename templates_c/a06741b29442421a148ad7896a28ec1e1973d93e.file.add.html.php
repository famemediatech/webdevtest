<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-23 14:20:09
         compiled from "C:\xampp\htdocs\ci\client\cp\default\famecms\modules\administrator\views\menu\users\add.html" */ ?>
<?php /*%%SmartyHeaderCode:22333576b8da9bd6ff5-53000928%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a06741b29442421a148ad7896a28ec1e1973d93e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\ci\\client\\cp\\default\\famecms\\modules\\administrator\\views\\menu\\users\\add.html',
      1 => 1465272825,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22333576b8da9bd6ff5-53000928',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_desc' => 0,
    'rolelist' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_576b8da9c42b46_91126445',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_576b8da9c42b46_91126445')) {function content_576b8da9c42b46_91126445($_smarty_tpl) {?>
<div ng-controller="AddUserCtrl">
 <div class="content-header">
                        <div class="header-section">
                            <h1>
                                <i class="fa fa-gear"></i><?php echo $_smarty_tpl->tpl_vars['page_desc']->value;?>

                            </h1>
                        </div>
 </div>
<!-- <ul class="breadcrumb breadcrumb-top">
    <li>Forms</li>
    <li><a href="">Components</a></li>
</ul>-->
<!-- END Components Header -->

<!-- Form Components Row -->
<div class="row">
    <div class="col-md-9">
        <!-- Select Components Block -->
        <div class="block">
            <!-- Select Components Content -->
             <form name="addUserForm" class="form-horizontal form-bordered " novalidate>
                <div class="form-group">
					<div class="col-xs-6">
						<div class="input-group" ng-class="
						{'has-error': addUserForm.firstname.$invalid && !addUserForm.firstname.$pristine,
						 'has-success': addUserForm.firstname.$valid}
						">
							<span class="input-group-addon"><i class="gi gi-user"></i></span>
							<input type="text" name="firstname" ng-model="dataForm.firstname" 
							class="form-control input-lg" placeholder="Firstname" required>
						</div>
						<span class="help-block" ng-show="addUserForm.firstname.$error.required && !addUserForm.firstname.$pristine">First Name cannot be blank</span>
					</div>
					<div class="col-xs-6">
						<input type="text" name="lastname" ng-model="dataForm.lastname" 
						 class="form-control input-lg" placeholder="Lastname">
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group" ng-class="
							{'has-error': addUserForm.email.$invalid && !addUserForm.email.$pristine,
							 'has-success': addUserForm.email.$valid}
							">
							<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
							<input type="email"  name="email" ng-model="dataForm.email"
							class="form-control input-lg" placeholder="Email" required email-available>
							<small ng-if="addUserForm.email.$pending" class="error">Checking Email...</small>
						</div>
							<span class="help-block" 
								ng-show="addUserForm.email.$error.required && !addUserForm.email.$pristine">
								Email cannot be blank
							</span>
							<span class="help-block" 
								ng-show="addUserForm.email.$error.email && !addUserForm.email.$pristine">
								Enter a valid email address
							</span>
							<span class="help-block" 
								ng-show="addUserForm.email.$error.unique && !addUserForm.email.$pristine">
								Email already exist.Choose another one!!
							</span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group" ng-class="
							{'has-error': addUserForm.username.$invalid && !addUserForm.username.$pristine,
							 'has-success': addUserForm.username.$valid}
							">
						<span class="input-group-addon">@</span>
							<input type="text"  name="username" ng-model="dataForm.username" 
							ng-minlength="5"
							ng-maxlength="10"
							class="form-control input-lg" placeholder="Username" username-available required>
						<small ng-if="addUserForm.username.$pending" class="error">Checking Username Availability...</small>
						</div>
						<span class="help-block" 
							ng-show="addUserForm.username.$error.required && !addUserForm.username.$pristine">
							Username cannot be blank
						</span>
						<span class="help-block"
							ng-show="addUserForm.username.$error.maxlength || addUserForm.username.$error.minlength 
							&& !addUserForm.username.$pristine">
							Username must be 5-10 chars
						</span>
						<span class="help-block" 
							ng-show="addUserForm.username.$error.unique && !addUserForm.username.$pristine">
							Username has been taken.Choose another one!!
						</span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group" ng-class="
							{'has-error': addUserForm.password.$invalid && !addUserForm.password.$pristine,
							 'has-success': addUserForm.password.$valid}
							">
							<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
							<input type="password" name="password" ng-model="dataForm.password"
							ng-minlength="5"
							ng-maxlength="20"
							class="form-control input-lg" placeholder="Password" required>
						</div>
							<span class="help-block" 
							ng-show="addUserForm.password.$error.required && !addUserForm.password.$pristine">
							Password cannot be blank
							</span>
							<span class="help-block"
							ng-show="addUserForm.password.$error.maxlength || addUserForm.password.$error.minlength 
							&& !addUserForm.password.$pristine">
							Password must be 5-20 chars
							</span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group" ng-class="
							{'has-error': addUserForm.pass2.$invalid && !addUserForm.pass2.$pristine,
							 'has-success': addUserForm.pass2.$valid}
							">
							<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
							<input type="password" name="pass2" ng-model="dataForm.pass2"
							password-match="dataForm.password"
							class="form-control input-lg" placeholder="Verify Password" required>
						</div>
							<span class="help-block"
								ng-show="addUserForm.pass2.$error.required
								&& !addUserForm.pass2.$pristine">
								Verify Password cannot be blank
							</span>
							<span class="help-block"
								ng-show="addUserForm.pass2.$error.unique
								&& !addUserForm.pass2.$pristine">
								Password is not match
							</span>
					</div>
				</div>
                
            <!-- END Select Components Content -->
        </div>
        <!-- END Select Components Block -->

       
    </div>
     <div class="col-md-3">
        <!-- Select Components Block -->
        <div class="block">
            <!-- Select Components Title -->
            <div class="block-title">
                <h2><strong>Manage</strong> </h2>
            </div>
            <!-- END Select Components Title -->

            <!-- Select Components Content -->
            <div class="form-group">
					<label for="role">Role</label>
					<select name="role" class="form-control" size="1" ng-model="dataForm.role" required>
						<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_smarty_tpl->tpl_vars['myId'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['rolelist']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
 $_smarty_tpl->tpl_vars['myId']->value = $_smarty_tpl->tpl_vars['i']->key;
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['i']->value['value'];?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value['name'];?>
</option>
						<?php } ?>
					</select>
				</div>
			<div class="form-group">
					<label for="status">Status</label>
					<select name="status" class="form-control" size="1" ng-model="dataForm.status">
						<option value="active">Active</option>
						<option value="not active">Not Active</option>
					</select>
			</div>
            <div class="form-group form-actions">
                    <button type="submit" ng-disabled="addUserForm.$invalid" ng-click="buttonAdd(dataForm)" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Create</button>
            </div>
           
            <!-- END Select Components Content -->
        </div>
        <!-- END Select Components Block -->
       
    </div>

  
     </form>
</div>
<!-- END Form Components Row -->
</div>
<?php echo $_smarty_tpl->getSubTemplate ("menu/setting/editdp-modal.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
