<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-23 14:20:05
         compiled from "C:\xampp\htdocs\ci\client\cp\default\famecms\modules\administrator\views\menu\users\index.html" */ ?>
<?php /*%%SmartyHeaderCode:15847576b79e60c6718-68860373%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f655bf1aadfaa4abfaa1cb91b2910ac9ac0c1d3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\ci\\client\\cp\\default\\famecms\\modules\\administrator\\views\\menu\\users\\index.html',
      1 => 1465832637,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15847576b79e60c6718-68860373',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_576b79e6120348_21852937',
  'variables' => 
  array (
    'page_desc' => 0,
    'items' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_576b79e6120348_21852937')) {function content_576b79e6120348_21852937($_smarty_tpl) {?>
 <!-- Datatables Header -->
					<div ng-controller="ManageUserCtrl">
                    <div class="content-header">
                        <div class="header-section">
                            <h1>
                                <i class="fa fa-table"></i>Table <?php echo $_smarty_tpl->tpl_vars['page_desc']->value;?>

                            </h1>
                        </div>
                    </div>
                    <!--<ul class="breadcrumb breadcrumb-top">
                        <li>Tables</li>
                        <li><a href="">Datatables</a></li>
                    </ul>-->
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                     
                    <div class="block full">
						<div align="right"  class="block-title">
						<div class="btn-group">
											<a href="<?php echo base_url('administrator/users/add');?>
" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
						</div>
						</div>
                        <div class="table-responsive">
                            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Username</th>
                                        <th class="text-center">Full Name</th>
                                        <th class="text-center">Role</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_smarty_tpl->tpl_vars['myId'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
 $_smarty_tpl->tpl_vars['myId']->value = $_smarty_tpl->tpl_vars['i']->key;
?>
                                    <tr>
                                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['i']->value['user_id'];?>
</td>
                                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['i']->value['user_name'];?>
</td>
                                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['i']->value['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['i']->value['last_name'];?>
</td>
                                        <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['i']->value['role'];?>
</td>
                                        <td class="text-center"><span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['i']->value['status'];?>
</span></td>
										<td class="text-center">
											<?php if ($_smarty_tpl->tpl_vars['i']->value['user_id']!=1) {?>
											<div class="btn-group">
												<a href="<?php echo base_url('administrator/users/edit');?>
/<?php echo $_smarty_tpl->tpl_vars['i']->value['user_id'];?>
" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
												<a ng-click="deletePermanentAction(<?php echo $_smarty_tpl->tpl_vars['i']->value['user_id'];?>
)" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
											</div>
											<?php }?>
										</td>
                                    </tr>
                                <?php } ?>   
                                </tbody>
                            </table>
							
                        </div>
                    </div>
                    <!-- END Datatables Content -->
					</div>

                   <?php }} ?>
