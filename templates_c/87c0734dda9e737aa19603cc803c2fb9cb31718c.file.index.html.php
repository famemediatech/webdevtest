<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-23 12:54:58
         compiled from "C:\xampp\htdocs\ci\client\cp\default\famecms\modules\administrator\views\menu\location\country\index.html" */ ?>
<?php /*%%SmartyHeaderCode:19378576b79b2875877-66085443%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '87c0734dda9e737aa19603cc803c2fb9cb31718c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\ci\\client\\cp\\default\\famecms\\modules\\administrator\\views\\menu\\location\\country\\index.html',
      1 => 1456634201,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19378576b79b2875877-66085443',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_desc' => 0,
    'items' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_576b79b28c60e6_71519990',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_576b79b28c60e6_71519990')) {function content_576b79b28c60e6_71519990($_smarty_tpl) {?>
 <!-- Datatables Header -->
<div ng-controller="ManageCountryCtrl">
<div class="content-header">
    <div class="header-section">
        <h1>
            <i class="fa fa-table"></i><?php echo $_smarty_tpl->tpl_vars['page_desc']->value;?>

        </h1>
    </div>
</div>
<!--<ul class="breadcrumb breadcrumb-top">
    <li>Tables</li>
    <li><a href="">Datatables</a></li>
</ul>-->
<!-- END Datatables Header -->

<!-- Datatables Content -->
 
<div class="block full">
<div align="right"  class="block-title">
<div class="btn-group">
                    <a href="<?php echo base_url('administrator/location/add_country');?>
" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
 </div>
</div>
    <div class="table-responsive">
       <table id="example-datatable" class="table table-vcenter table-condensed">
            <thead>
                <tr>
                    <th class="text-center">Country Name</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            <?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_smarty_tpl->tpl_vars['myId'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
 $_smarty_tpl->tpl_vars['myId']->value = $_smarty_tpl->tpl_vars['i']->key;
?>
                <tr>
                    <td class="text-center">
                    <a href="<?php echo base_url('administrator/location/list_state');?>
/<?php echo $_smarty_tpl->tpl_vars['i']->value['id_country'];?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value['country_name'];?>
</a>
                    </td>
                    <td class="text-center"><span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['i']->value['status'];?>
</span></td>
                    <td class="text-center">
                        <div class="btn-group">
                        <a href="<?php echo base_url('administrator/location/edit_country');?>
/<?php echo $_smarty_tpl->tpl_vars['i']->value['id_country'];?>
" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                        <a ng-click="deletePermanentAction(<?php echo $_smarty_tpl->tpl_vars['i']->value['id_country'];?>
)" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>    
                    </div>
                    </td>
                </tr>
             <?php } ?>
            </tbody>
        </table>
		
    </div>
</div>
<!-- END Datatables Content -->
</div>

                   <?php }} ?>
