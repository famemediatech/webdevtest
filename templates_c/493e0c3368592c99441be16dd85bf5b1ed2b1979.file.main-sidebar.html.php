<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-06-24 01:32:03
         compiled from "C:\xampp\htdocs\ci\client\cp\default\famecms\modules\administrator\views\partials\main-sidebar.html" */ ?>
<?php /*%%SmartyHeaderCode:21829576b78fa7b1416-44505178%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '493e0c3368592c99441be16dd85bf5b1ed2b1979' => 
    array (
      0 => 'C:\\xampp\\htdocs\\ci\\client\\cp\\default\\famecms\\modules\\administrator\\views\\partials\\main-sidebar.html',
      1 => 1466706721,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21829576b78fa7b1416-44505178',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_576b78fa7d97f0_93955965',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_576b78fa7d97f0_93955965')) {function content_576b78fa7d97f0_93955965($_smarty_tpl) {?><div id="sidebar" ng-controller="sidebarCtrl">
	<!-- Wrapper for scrolling functionality -->
	<div class="sidebar-scroll">
		<!-- Sidebar Content -->
		<div class="sidebar-content">
			<!-- Brand -->
			<a href="<?php echo base_url();?>
" class="sidebar-brand">
				<strong>Fame</strong>CMS
			</a>
			<!-- END Brand -->

			<!-- User Info -->
			<div class="sidebar-section sidebar-user clearfix">
				<div class="sidebar-user-avatar">
					<a href="">
            		<img style="-webkit-border-radius: 32px;-moz-border-radius: 32px;border-radius: 32px;height:64px;width:64px;" src="<?php echo base_url();?>
{{dataadmin.display_picture ? dataadmin.display_picture:'public/img/unknown.png'}}">
					</a>
				</div>
				<div class="sidebar-user-name">{{dataadmin.first_name}} {{dataadmin.last_name}}</div>
				<div class="sidebar-user-links">
					<a href="<?php echo base_url('administrator/setting/profile');?>
" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
					<!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
					<a href="<?php echo base_url('administrator/setting/');?>
" data-toggle="tooltip" data-placement="bottom" title="Settings"><i class="gi gi-cogwheel"></i></a>
					<a href="<?php echo base_url('administrator/logout/');?>
" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
				</div>
			</div>
			<!-- END User Info -->
			<!-- Sidebar Navigation -->
			<ul class="sidebar-nav">
				<li class="sidebar-header">
					<span class="sidebar-header-title">Site</span>
				</li>
				<li <?php if (checkUri('2')=='') {?>class="active" <?php }?>>
					<a href="<?php echo base_url('administrator/');?>
"><i class="gi gi-home sidebar-nav-icon"></i>Dashboard</a>
				</li>
				<li class="sidebar-header">
					<span class="sidebar-header-title">Module</span>
				</li>
				<li <?php if (checkUri('2')=='page') {?>class="active" <?php }?>>
					<a href="<?php echo base_url('administrator/page/');?>
"><i class="gi gi-file sidebar-nav-icon"></i>Page</a>
				</li>
				<li <?php if (checkUri('2')=='post') {?>class="active" <?php }?>>
					<a href="<?php echo base_url('administrator/post/');?>
"><i class="gi gi-pushpin sidebar-nav-icon"></i>Post</a>
				</li>
				<li <?php if (checkUri('2')=='category') {?>class="active" <?php }?>>
					<a href="<?php echo base_url('administrator/category/');?>
"><i class="gi gi-list sidebar-nav-icon"></i>Category</a>
				</li>
				
				 <li class="sidebar-header">
					<span class="sidebar-header-title">User</span>
				</li>
				<li <?php if (checkUri('2')=='users') {?>class="active" <?php }?>>
				<a href="<?php echo base_url('administrator/users/');?>
" ><i class="fa fa-user sidebar-nav-icon"></i>User</a>
				</li>
				<li class="sidebar-header">
					<span class="sidebar-header-title">Setting</span>
				</li>
				<li <?php if (checkUri('2')=='setting') {?>class="active" <?php }?>>
					<a href="<?php echo base_url('administrator/setting/');?>
"><i class="fa fa-globe sidebar-nav-icon"></i>Website Setting</a>
				</li>
				<li <?php if (checkUri('2')=='location') {?>class="active" <?php }?>>
					<a href="<?php echo base_url('administrator/location/');?>
"><i class="fa fa-map-marker sidebar-nav-icon"></i>Location</a>
				</li>
			</ul>
			<!-- END Sidebar Navigation -->

			
		</div>
		<!-- END Sidebar Content -->
	</div>
	<!-- END Wrapper for scrolling functionality -->
</div><?php }} ?>
