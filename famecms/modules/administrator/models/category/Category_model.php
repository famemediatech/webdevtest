<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Mini-Developer version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.0
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Category_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('phpass');

    }

public function create_new(
			$name,$slug,$status,$type,$parent
			){
			//$str = str_replace(" ", "-", $name);
		 	$slugs = strtolower($slug);

			$data=array(
			'name_cat'=>$name,
			'cat_status'=>$status,
			'cat_type'=>$type,
			'slug'=>$slugs,
			'id_parent'=>$parent,
			);
			$create = $this->db->insert('fame_category',$data);
			if ($create){
				return true;
			} else {
				return false;
			}
		}

public function update_set(
			$pid,$name,$status,$type,$parent){
			$data=array(
			'name_cat'=>$name,
			'cat_status'=>$status,
			'cat_type'=>$type,
			'slug'=>$slug,
			'id_parent'=>$parent,
			);
			$this->db->where('id_cat',$pid);
			$update = $this->db->update('fame_category',$data);
			if ($update){
				return true;
			} else {
				return false;
			}
		}
public function delete_permanent_content($id){
			$this->db->where('id_cat', $id);
			$delete_permanent = $this->db->delete('fame_category'); 
			if ($delete_permanent){
				return true;
			} else {
				return false;
			}
		}

//get Json
public function getTableList()
		{	 
			$this->db->select('A.id_cat AS id_category,A.name_cat AS category_name,A.cat_status AS status,B.name_cat AS parent_name');
            $this->db->from('fame_category AS A');
            $this->db->join('fame_category AS B', 'B.id_cat = A.id_parent','left');
            $query=$this->db->get();
            $result = $query->result_array();
			return $result;
		}
public function getParentList()
		{	
			$this->db->select('A.id_cat AS value,A.name_cat AS name');
			$this->db->where('A.id_parent','0');
			$this->db->where('A.cat_type','product');
			$this->db->where('A.cat_status','active');
            $this->db->from('fame_category AS A');
            $query=$this->db->get();
            $result = $query->result_array();
			return $result;
		}
public function getOldData($pid)
		{	
			$this->db->select('A.name_cat name,A.cat_type AS type,A.cat_status AS status,A.id_parent AS parent');
            $this->db->from('fame_category AS A');
            $this->db->where('A.id_cat',$pid);
            $query=$this->db->get();
            $result = $query->result();
			return $result;
		}
public function check_slug($slug)
		{
			$this->db->where("slug",$slug);
			$query=$this->db->get("fame_category");
			if($query->num_rows()>0)
			{
				return true;
			}
				return false;
		}
}
