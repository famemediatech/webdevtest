<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Mini-Developer version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.0
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Order_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('phpass');

    }

public function create_new(
      $table_no,$transID,$order_detail,$employee_id
			){

			$data=array(
        'trans_id'=>$transID,
        'table_no'=>$table_no,
        'order_detail'=>$order_detail,
        'status'=>1,
        'employee_id'=>$employee_id,
			);
			$create = $this->db->insert('fame_order',$data);
			if ($create){
				return true;
			} else {
				return false;
			}
		}

public function update_set(
      $pid,$table_no,$order_detail){
      $data=array(
        'table_no'=>$table_no,
			  'order_detail'=>$order_detail
      );
			$this->db->where('id',$pid);
			$update = $this->db->update('fame_order',$data);
			if ($update){
				return true;
			} else {
				return false;
			}
    }
    
public function delete_permanent_content($id){
			$this->db->where('id', $id);
			$delete_permanent = $this->db->delete('fame_menu'); 
			if ($delete_permanent){
				return true;
			} else {
				return false;
			}
    }

  public function create_payment(
    $pid,$employee_id,$payment_detail
    ){

    $data=array(
      'order_id'=>$pid,
      'payment_detail'=>$payment_detail,
      'employee_id'=>$employee_id,
    );
    $create = $this->db->insert('fame_payment',$data);
    if ($create){
      return true;
    } else {
      return false;
    }
  }

  public function update_status(
    $pid){
    $data=array(
      'status'=> 2
    );
    $this->db->where('id',$pid);
    $update = $this->db->update('fame_order',$data);
    if ($update){
      return true;
    } else {
      return false;
    }
  }

//get Json
public function getTableList()
		{	 
      $this->db->select('A.id AS id,
      A.trans_id AS trans_id,
      A.table_no AS table_no,
      A.employee_id AS employee_id,
      A.createdOn AS createdOn,
      (CASE A.status
        WHEN 1 THEN "Unpaid" 
        WHEN 2 THEN "Paid" 
        ELSE "Unpaid" 
      END) AS status');
      $this->db->from('fame_order AS A');
      $query=$this->db->get();
      $result = $query->result_array();
			return $result;
		}
public function getOldData($pid)
		{	
      $this->db->select('A.id AS order_id,
      A.trans_id AS trans_id,
      A.table_no AS name,
      A.order_detail AS order_detail,
      A.status AS status,
      A.employee_id AS employee_id,
      A.createdOn AS createdOn,
      B.payment_detail AS payment_detail');
      $this->db->from('fame_order AS A');
      $this->db->join('fame_payment AS B','B.order_id = A.id','left');
      $this->db->where('A.id',$pid);
      $query=$this->db->get();
      $result = $query->result();
			return $result;
    }
public function checkTransID()
		{
			$this->db->where("trans_id",'ERP'.date('dmY').'-001');
			$query=$this->db->get("fame_order");
			if($query->num_rows()>0)
			{
				return true;
			}
				return false;
    }
    public function getLastTransID()
  {
    $this->db->like("trans_id",'ERP'.date('dmY').'-');
    $this->db->order_by('trans_id','desc');
    $this->db->limit(1);
    $query=$this->db->get("fame_order");
    $result = $query->result_array();
    return $result;
  }
}