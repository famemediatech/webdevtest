<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Mini-Developer version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.0
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Location_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('phpass');

    }

public function create_new_country($name,$status){
			$data=array(
			'name'=>$name,
			'location_type'=>0,
			'parent_id'=>0,
			'is_visible'=>$status
			);
			$create = $this->db->insert('location',$data);
			if ($create){
				return true;
			} else {
				return false;
			}
		}

public function update_set_country($pid,$name,$status){
			$data=array(
			'name'=>$name,
			'is_visible'=>$status
			);
			$this->db->where('location_id',$pid);
			$update = $this->db->update('location',$data);
			if ($update){
				return true;
			} else {
				return false;
			}
		}
public function delete_permanent_country($id){
			$this->db->where('location_id', $id);
			$delete_permanent = $this->db->delete('location'); 
			if ($delete_permanent){
				return true;
			} else {
				return false;
			}
		}

//get Json
public function getCountryList()
		{	
			$this->db->select('A.location_id AS id_country,A.name AS country_name,A.is_visible AS status');
            $this->db->from('location AS A');
            $this->db->where('location_type',0);
            $query=$this->db->get();
            $result = $query->result_array();
			return $result;
		}
public function getStateList($country)
		{	
			$this->db->select('A.location_id AS id_state,A.name AS state_name,A.is_visible AS status');
            $this->db->from('location AS A');
            $this->db->where('location_type',1);
            $this->db->where('parent_id',$country);
            $query=$this->db->get();
            $result = $query->result_array();
			return $result;
		}
public function getOldDataCountry($pid)
		{	
			$this->db->select('A.location_id AS id_country,A.name AS name,A.is_visible AS status');
            $this->db->from('location AS A');
            $this->db->where('A.location_id',$pid);
            $query=$this->db->get();
            $result = $query->result();
			return $result;
		}
}
