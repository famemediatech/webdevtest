<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Mini-Developer version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.0
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Menu_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('phpass');

    }

public function create_new(
			$name,$status,$category,$price
			){

			$data=array(
			'menu_name'=>$name,
			'menu_status'=>$status,
			'menu_category'=>$category,
			'menu_price'=>$price,
			);
			$create = $this->db->insert('fame_menu',$data);
			if ($create){
				return true;
			} else {
				return false;
			}
		}

public function update_set(
			$pid,$name,$status,$category,$price){
      $data=array(
        'menu_name'=>$name,
        'menu_status'=>$status,
        'menu_category'=>$category,
        'menu_price'=>$price,
      );
			$this->db->where('id',$pid);
			$update = $this->db->update('fame_menu',$data);
			if ($update){
				return true;
			} else {
				return false;
			}
		}
public function delete_permanent_content($id){
			$this->db->where('id', $id);
			$delete_permanent = $this->db->delete('fame_menu'); 
			if ($delete_permanent){
				return true;
			} else {
				return false;
			}
		}

//get Json
public function getTableList()
		{	 
      $this->db->select('A.id AS menu_id,A.menu_name AS name,
      (CASE A.menu_category
        WHEN 1 THEN "Food" 
        WHEN 2 THEN "Beverage" 
        ELSE "Food" 
      END) as category,
      A.menu_price AS price,
      (CASE A.menu_status
        WHEN 0 THEN "Not Active" 
        WHEN 1 THEN "Active" 
        ELSE "Not Active" 
      END) AS status');
      $this->db->from('fame_menu AS A');
      $query=$this->db->get();
      $result = $query->result_array();
			return $result;
		}
public function getOldData($pid)
		{	
			$this->db->select('A.id AS id_menu,A.menu_name AS name,A.menu_category AS category,A.menu_price AS price,A.menu_status AS status');
      $this->db->from('fame_menu AS A');
      $this->db->where('A.id',$pid);
      $query=$this->db->get();
      $result = $query->result();
			return $result;
		}
	public function getActiveMenu()
	{	
		$this->db->select('A.id AS value,A.menu_name AS name,A.menu_price AS price');
		$this->db->from('fame_menu AS A');
		$this->db->where('A.menu_status',1);
		$query=$this->db->get();
		$result = $query->result_array();
		return $result;
	}
}