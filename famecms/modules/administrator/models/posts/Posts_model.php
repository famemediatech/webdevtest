<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Client version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.2
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Posts_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('admin');
    }

// Get Data Default Form
public function getCategoryList()
		{	
			$this->db->select('name_cat AS name,id_cat AS value');
			$this->db->where('cat_type', 'post'); 
            $this->db->where('id_parent', '0');
            $query=$this->db->get('fame_category');
            $result = $query->result_array();
			return $result;
		}
public function getSidebarList(){
			$this->db->select('widget_name AS name,id_widget AS value');
			$query=$this->db->get('fame_widget');
	        $result = $query->result_array(); 
	        return $result;
		}

// Processing Form
public function create_new($title,$content,$slug,
				$custom_seo,$seo_index,$seo_follow,$seo_title,$seo_keyword,$seo_desc,
				$publish,$custom_date,$publish_date,
				$category,$feat_img,$sidebar,$sidebar_name,$comment,$ordernum){
				if($custom_date =='yes'){
					$date = $publish_date;
				} else {
					$date = date('Y-m-d H:i:s');
				}
			//$str = str_replace(" ", "-", $title);
			//$str = format_uri($title);
			//$uniqueID = createUniqueID(6,'ID','fame_post');
			$data=array(
			'post_author'=>$this->session->userdata('admin_id'),
			'post_date'=>$date,
			'post_title'=>$title,
			'post_content'=>$content,
			'post_excerpt'=>'',
			'post_status'=>$publish,
			'comment_status'=>$comment,
			'post_parent'=>$category,
			'feat_img_url'=>$feat_img,
			'post_url'=>strtolower ( $slug ),
			'menu_order'=>$ordernum,
			'post_type'=>"post",
			'sidebar_type'=>$sidebar,
			'sidebar_id'=>$sidebar_name,
			// Seo data
			'custom_seo'=>$custom_seo,
			'seo_title'=>$seo_title,
			'seo_description'=>$seo_desc,
			'seo_keyword'=>$seo_keyword,
			'seo_index'=>$seo_index,
			'seo_follow'=>$seo_follow,
			);
			$create = $this->db->insert('fame_post',$data);
			if ($create){
				return true;
			} else {
				return false;
			}
		}
		
public function update_set($pid,$title,$slug,$content,
				$custom_seo,$seo_index,$seo_follow,$seo_title,$seo_keyword,$seo_desc,
				$publish,$new_date,
				$category,$feat_img,$sidebar,$sidebar_name,$comment,$ordernum){
			//$str = str_replace(" ", "-", $slug);
			
			//$uniqueID = createUniqueID(6,'ID','fame_post');
			if ($new_date !=''){
			$data=array(
			'post_title'=>$title,
			'post_content'=>$content,
			'post_excerpt'=>'',
			'post_status'=>$publish,
			'comment_status'=>$comment,
			'post_date'=>date('Y-m-d H:i:s',strtotime($new_date)),
			'post_modified'=>date('Y-m-d H:i:s'),
			'post_parent'=>$category,
			'feat_img_url'=>$feat_img,
			'menu_order'=>$ordernum,
			'sidebar_type'=>$sidebar,
			'sidebar_id'=>$sidebar_name,
			// Seo data
			'custom_seo'=>$custom_seo,
			'seo_title'=>$seo_title,
			'seo_description'=>$seo_desc,
			'seo_keyword'=>$seo_keyword,
			'seo_index'=>$seo_index,
			'seo_follow'=>$seo_follow,
			);
			} else {
			$data=array(
			'post_title'=>$title,
			'post_content'=>$content,
			'post_excerpt'=>'',
			'post_status'=>$publish,
			'comment_status'=>$comment,
			'post_modified'=>date('Y-m-d H:i:s'),
			'post_parent'=>$category,
			'feat_img_url'=>$feat_img,
			'menu_order'=>$ordernum,
			'sidebar_type'=>$sidebar,
			'sidebar_id'=>$sidebar_name,
			// Seo data
			'custom_seo'=>$custom_seo,
			'seo_title'=>$seo_title,
			'seo_description'=>$seo_desc,
			'seo_keyword'=>$seo_keyword,
			'seo_index'=>$seo_index,
			'seo_follow'=>$seo_follow,
			);
			}
			
			$this->db->where('ID',$pid);
			$update = $this->db->update('fame_post',$data);
			if ($update){
				return true;
			} else {
				return false;
			}
		}

public function delete_content($id){
			$data=array(
			'post_status'=>'delete',
			);
			$this->db->where('ID', $id);
			$delete = $this->db->update('fame_post', $data);
			if ($delete){
				return true;
			} else {
				return false;
			} 
		}

public function restore_content($id){
			$data=array(
			'post_status'=>'publish',
			);
			$this->db->where('ID', $id);
			$restore = $this->db->update('fame_post', $data); 
			if ($restore){
				return true;
			} else {
				return false;
			}
		}

public function delete_permanent_content($id){
			$this->db->where('ID', $id);
			$delete_permanent = $this->db->delete('fame_post'); 
			if ($delete_permanent){
				return true;
			} else {
				return false;
			}
		}

//Get Json Data 
public function getTableList()
		{	
			$this->db->select('A.ID AS id_post,A.post_title AS title,A.post_date AS date,A.post_modified AS modified,A.post_status AS status,B.user_name AS author');
            $this->db->from('fame_post AS A');
            $this->db->where('A.post_type','post');
            $where = "A.post_status != 'delete'";
            $this->db->where($where);
            $this->db->join('fame_user AS B', 'B.user_id = A.post_author');
            $query=$this->db->get();
            $result = $query->result_array();
			return $result;
		}
		
public function getTrashList()
		{	
			$this->db->select('A.ID AS id_post,A.post_title AS title,A.post_date AS date,A.post_modified AS modified,A.post_status AS status,B.user_name AS author');
            $this->db->from('fame_post AS A');
            $this->db->where('A.post_type','post');
            $where = "A.post_status = 'delete'";
            $this->db->where($where);
            $this->db->join('fame_user AS B', 'B.user_id = A.post_author');
            $query=$this->db->get();
            $result = $query->result_array();
			return $result;
		}
		
public function getOldData($pid)
		{	
			$this->db->select('A.ID AS id_post,A.post_title AS title,A.post_content AS content,A.post_url AS slug,
			A.custom_seo AS custom_seo,A.seo_index AS seo_index,A.seo_follow AS seo_follow,A.seo_title AS seo_title,A.seo_keyword AS seo_keyword,A.seo_description AS seo_desc,
			A.post_status AS publish,A.post_date AS publish_date,
			A.post_parent AS category,A.feat_img_url AS feat_img,A.sidebar_type AS sidebar,A.sidebar_id AS sidebar_name,A.comment_status AS comment,A.menu_order AS ordernum');
            $this->db->from('fame_post AS A');
            $this->db->where('A.ID',$pid);
            $query=$this->db->get();
            $result = $query->result();
			return $result;
		}
public function check_slug($slug)
		{
			$this->db->where("post_url",$slug);
			$query=$this->db->get("fame_post");
			if($query->num_rows()>0)
			{
				return true;
			}
				return false;
		}
}