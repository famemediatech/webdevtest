<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Mini-Developer version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.0
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Users extends CI_Controller {
	public $page_name = "Users";
    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        $this->load->library('parser');
        $this->load->helper('admin');
        $this->load->helper('phpass');
    }

    public function index()
    {
       if($this->session->has_userdata('admin_id'))
		{
			$this->_Userindex();
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
		}
	}
	public function input_data()
	{
				$this->load->model('administrator/user/user_model');

				$postdata = file_get_contents("php://input"); // Get Json Data
				if ($postdata){
			    $request = json_decode($postdata); // Decode Json Data
				
				//Main Data
			    $f_name = $this->security->xss_clean($request->firstname);
			    $l_name = $this->security->xss_clean(isset($request->lastname)?$request->lastname:'');
			    $email = $this->security->xss_clean($request->email);
			    $username = $this->security->xss_clean($request->username);
			    $pass = $this->security->xss_clean($request->password);
			    $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
    			$hash_password = $hasher->HashPassword($pass);
				
				//Publish Data
				$status = $this->security->xss_clean($request->status);
				$role = $this->security->xss_clean($request->role);
				
			    $action=$this->user_model->create_new(
				$f_name,$l_name,$email,$username,$hash_password,$status,$role);
				    if($action){
				    	echo $result = '{"status" : "success","message" : "New User has been succesfully created!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to create a new user!! Please try again later!!"}';
				    }
				} else {
		    		show_404('page');
		    	}
	}
	// Add Controller Function
	 public function add()
    {

    	if($this->session->has_userdata('admin_id'))
		{
			$this->_add_page();
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }

    public function _Userindex(){
    	// Some example data
		$data['title']=$this->page_name;
		$data['heading']="List ".$this->page_name;
		$data['page_desc']="List ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		//$loadJSFiles[] = base_url('public/js/admin/proui/page.js');
		//$loadJSFiles[] = base_url('themes/proui/js/ckeditor/ckeditor.js');
		$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
        // Load the template from the views directory
        $this->load->model('administrator/user/user_model');
		$getData=$this->user_model->getTableList();
		$this->smarty->assign('items', $getData);
		$data['content'] = "menu/users/index.html";
        $this->parser->parse("layout/main.html",$data);
    }
	
	//Add Page
	public function _add_page(){
    	 // Some example data
		$data['title']="Create New ".$this->page_name;
		$data['heading']="Create New ".$this->page_name;
		$data['page_desc']="Form New ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('themes/proui/js/ckeditor/ckeditor.js');
		//$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		//$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
		
        // Load the template from the views directory
        $this->load->model('administrator/user/user_model');
		
		$getRole=$this->user_model->getRoleList();
		$this->smarty->assign('rolelist', $getRole);
		
		$data['content'] = "menu/users/add.html";
        $this->parser->parse("layout/main.html",$data);
    }
}
