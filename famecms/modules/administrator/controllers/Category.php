<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Client version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.2
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Category extends CI_Controller {
	
	public $page_name = "Category";
    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        $this->load->library('parser');
        $this->load->library('security');
    }
	
	
    /*============================================
				Start Function Page Controller
	==============================================*/
	
	// Index Controller Function
    public function index()
    {

    	if($this->session->has_userdata('admin_id'))
		{
			$this->_index_page();
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
	
	// Add Controller Function
	 public function add()
    {

    	if($this->session->has_userdata('admin_id'))
		{
			$this->_add_page();
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
	
	// Edit Controller Function
	 public function edit($category_id ='NULL')
    {

    	if($this->session->has_userdata('admin_id'))
		{
			if ($category_id){
			$this->_edit_page($category_id);
			} else {
			show_404('page');
			}
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
	
	
	// Get Old Data Controller
	 public function getOldData()
    {
			$id=$this->session->userdata('admin_id');
			$pid = $this->uri->segment(4);
			if (isset($id) && isset($pid))
			{
				$this->load->model('administrator/category/category_model');
				$getData=$this->category_model->getOldData($pid);
				echo json_encode($getData);
			} else {
				show_404('page');
			}
    }
	
	
	/*============================================
				End Function Page Controller
	==============================================*/

	
    /*============================================
				Start Function AngularJS
	==============================================*/
	 public function input_data()
	{
				$this->load->model('administrator/category/category_model');

				$postdata = file_get_contents("php://input"); // Get Json Data
				if ($postdata){
			    $request = json_decode($postdata); // Decode Json Data
				
				//Main Data
			    $name = $this->security->xss_clean($request->name);
				$format_slug = format_uri($name);
				
				//Publish Data
				$status = $this->security->xss_clean($request->status);
				$type = $this->security->xss_clean($request->type);
				$parent = $this->security->xss_clean($request->parent);
				
				$check_slug =$this->category_model->check_slug($format_slug);
				if ($check_slug){
				$slug = increment_string($format_slug,'-');
				} else {
				$slug = $format_slug;
				}
				
			    $action=$this->category_model->create_new(
				$name,$slug,$status,$type,$parent
				);
				    if($action){
				    	echo $result = '{"status" : "success","message" : "New category has been succesfully created!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to create a new category!! Please try again later!!"}';
				    }
				} else {
		    		show_404('page');
		    	}
	}
	
	 public function update_data()
	{
				$this->load->model('administrator/category/category_model');

				$postdata = file_get_contents("php://input"); // Get Json Data
				if ($postdata){
			    $request = json_decode($postdata); // Decode Json Data
				
				//Main Data
				$pid = $this->security->xss_clean($request->id_post);
				$name = $this->security->xss_clean($request->name);
				
				//Publish Data
				$status = $this->security->xss_clean($request->status);
				$type = $this->security->xss_clean($request->type);
				$parent = $this->security->xss_clean($request->parent);
			    
				
			    $action=$this->category_model->update_set(
				$pid,$name,$status,$type,$parent);
				    if($action){
				    	echo $result = '{"status" : "success","message" : "Category has been succesfully updated!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to update category!! Please try again later!!"}';
				    }
				} else {
		    		show_404('page');
		    	}
	}
	
	
    public function delete_permanent_data()
    {
			$id=$this->session->userdata('admin_id');
			$pid=$this->uri->segment('4');
			if (isset($id) && isset($pid))
			{
				$this->load->model('administrator/category/category_model');
				$action=$this->category_model->delete_permanent_content($pid);
					if($action){
				    	echo $result = '{"status" : "success","message" : "Category has been succesfully deleted!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to delete category!! Please try again later!!"}';
				    }
			} else {
				show_404('page');
			}
    }
	/*============================================
				End Function AngularJS
	==============================================*/
	
	
	/*============================================
				Start Function View Page
	==============================================*/
	
	//Index Page
	 public function _index_page(){
    	 // Some example data
		$data['title']=$this->page_name;
		$data['heading']="List ".$this->page_name;
		$data['page_desc']="List Active ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/category.js');
		$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
        // Load the template from the views directory
        $this->load->model('administrator/category/category_model');
		$getData=$this->category_model->getTableList();
		$this->smarty->assign('items', $getData);
		$data['content'] = "menu/category/index.html";
        $this->parser->parse("layout/main.html",$data);
    }
	
	//Add Page
	public function _add_page(){
    	 // Some example data
		$data['title']="Create New ".$this->page_name;
		$data['heading']="Create New ".$this->page_name;
		$data['page_desc']="Form New ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/category.js');
		//$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		//$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
		
        // Load the template from the views directory
        $this->load->model('administrator/category/category_model');
		$getParent=$this->category_model->getParentList();
		$this->smarty->assign('parentlist', $getParent);
		$data['content'] = "menu/category/add.html";
        $this->parser->parse("layout/main.html",$data);
    }
	
	public function _edit_page($category_id){
    	 // Some example data
		$data['title']="Edit Data ".$this->page_name;
		$data['heading']="Edit Data ".$this->page_name;
		$data['page_desc']="Form Edit Data ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/category.js');
		//$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		//$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
		
        // Load the template from the views directory
        $this->load->model('administrator/category/category_model');
		$getParent=$this->category_model->getParentList();
		$this->smarty->assign('parentlist', $getParent);
		$this->smarty->assign('category_id', $category_id);
		$data['content'] = "menu/category/edit.html";
        $this->parser->parse("layout/main.html",$data);
    }
	
	/*============================================
				End Function View Page
	==============================================*/

}
