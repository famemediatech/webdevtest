<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Client version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.2
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Menu extends CI_Controller {
	
	public $page_name = "Menu";
    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        $this->load->library('parser');
        $this->load->library('security');
    }
	
	
    /*============================================
				Start Function Page Controller
	==============================================*/
	
	// Index Controller Function
    public function index()
    {

    	if($this->session->has_userdata('admin_id'))
		{
			$this->_index_page();
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
	
	// Add Controller Function
	 public function add()
    {

    	if($this->session->has_userdata('admin_id'))
		{
			$this->_add_page();
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
	
	// Edit Controller Function
	 public function edit($menu_id ='NULL')
    {

    	if($this->session->has_userdata('admin_id'))
		{
			if ($menu_id){
			$this->_edit_page($menu_id);
			} else {
			show_404('page');
			}
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
	
	
	// Get Old Data Controller
	 public function getOldData()
    {
			$id=$this->session->userdata('admin_id');
			$pid = $this->uri->segment(4);
			if (isset($id) && isset($pid))
			{
				$this->load->model('administrator/menu/menu_model');
				$getData=$this->menu_model->getOldData($pid);
				echo json_encode($getData);
			} else {
				show_404('page');
			}
    }
	
	
	/*============================================
				End Function Page Controller
	==============================================*/

	
    /*============================================
				Start Function AngularJS
	==============================================*/
	 public function input_data()
	{
				$this->load->model('administrator/menu/menu_model');

				$postdata = file_get_contents("php://input"); // Get Json Data
				if ($postdata){
			    $request = json_decode($postdata); // Decode Json Data
				
				//Main Data
			  $name = $this->security->xss_clean($request->name);
				
				//Publish Data
				$status = $this->security->xss_clean($request->status);
				$category = $this->security->xss_clean($request->category);
				$price = $this->security->xss_clean($request->price);
				
			  $action=$this->menu_model->create_new(
				$name,$status,$category,$price
				);
				    if($action){
				    	echo $result = '{"status" : "success","message" : "New menu has been succesfully created!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to create a new menu!! Please try again later!!"}';
				    }
				} else {
		    		show_404('page');
		    	}
	}
	
	 public function update_data()
	{
				$this->load->model('administrator/menu/menu_model');

				$postdata = file_get_contents("php://input"); // Get Json Data
				if ($postdata){
			    $request = json_decode($postdata); // Decode Json Data
				
				//Main Data
				$pid = $this->security->xss_clean($request->id_menu);
				$name = $this->security->xss_clean($request->name);
				
				//Publish Data
				$status = $this->security->xss_clean($request->status);
				$category = $this->security->xss_clean($request->category);
				$price = $this->security->xss_clean($request->price);
			    
				
			  $action=$this->menu_model->update_set(
				$pid,$name,$status,$category,$price);
				    if($action){
				    	echo $result = '{"status" : "success","message" : "Menu has been succesfully updated!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to update Menu!! Please try again later!!"}';
				    }
				} else {
		    		show_404('page');
		    	}
	}
	
	
    public function delete_permanent_data()
    {
			$id=$this->session->userdata('admin_id');
			$pid=$this->uri->segment('4');
			if (isset($id) && isset($pid))
			{
				$this->load->model('administrator/menu/menu_model');
				$action=$this->menu_model->delete_permanent_content($pid);
					if($action){
				    	echo $result = '{"status" : "success","message" : "Menu has been succesfully deleted!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to delete Menu!! Please try again later!!"}';
				    }
			} else {
				show_404('page');
			}
    }
	/*============================================
				End Function AngularJS
	==============================================*/
	
	
	/*============================================
				Start Function View Page
	==============================================*/
	
	//Index Page
	 public function _index_page(){
    	 // Some example data
		$data['title']=$this->page_name;
		$data['heading']="List ".$this->page_name;
		$data['page_desc']="List Active ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/menu.js');
		$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
        // Load the template from the views directory
    $this->load->model('administrator/menu/menu_model');
		$getData=$this->menu_model->getTableList();
		$this->smarty->assign('items', $getData);
		$data['content'] = "menu/menu/index.html";
        $this->parser->parse("layout/main.html",$data);
    }
	
	//Add Page
	public function _add_page(){
    	 // Some example data
		$data['title']="Create New ".$this->page_name;
		$data['heading']="Create New ".$this->page_name;
		$data['page_desc']="Form New ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/menu.js');
		//$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		//$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
		
    // Load the template from the views directory
		$data['content'] = "menu/menu/add.html";
    $this->parser->parse("layout/main.html",$data);
    }
	
	public function _edit_page($menu_id){
    	 // Some example data
		$data['title']="Edit Data ".$this->page_name;
		$data['heading']="Edit Data ".$this->page_name;
		$data['page_desc']="Form Edit Data ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/menu.js');
		//$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		//$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
		
    // Load the template from the views directory
		$this->smarty->assign('menu_id', $menu_id);
		$data['content'] = "menu/menu/edit.html";
        $this->parser->parse("layout/main.html",$data);
    }
	
	/*============================================
				End Function View Page
	==============================================*/

}
