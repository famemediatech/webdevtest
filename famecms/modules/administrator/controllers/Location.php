<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Client version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.2
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Location extends CI_Controller {
	
	public $page_name = "Location";
    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        $this->load->library('parser');
        $this->load->library('security');
    }
	
	
    /*============================================
				Start Function Location Controller
	==============================================*/
	
	// Index Controller Function
    public function index()
    {

    	if($this->session->has_userdata('admin_id'))
		{
			$this->_index_page();
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
	
	// Add Country Controller Function
	 public function add_country()
    {

    	if($this->session->has_userdata('admin_id'))
		{
			$this->_add_country_page();
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
	
	// Edit Controller Function
	 public function edit_country($country_id ='NULL')
    {

    	if($this->session->has_userdata('admin_id'))
		{
			if ($country_id){
			$this->_edit_country_page($country_id);
			} else {
			show_404('page');
			}
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
	
	
	// Get Old Data Controller
	 public function getOldDataCountry()
    {
			$id=$this->session->userdata('admin_id');
			$pid = $this->uri->segment(4);
			if (isset($id) && isset($pid))
			{
				$this->load->model('administrator/location/location_model');
				$getData=$this->location_model->getOldDataCountry($pid);
				echo json_encode($getData);
			} else {
				show_404('page');
			}
    }
	
	/*============================================*/
		// Index State Controller Function
	    public function list_state($country_id ='NULL')
    {

    	if($this->session->has_userdata('admin_id'))
		{
			if ($country_id){
			$this->_list_state_page($country_id);
			} else {
			show_404('page');
			}
		} else {
			// redirect them to the login page
			redirect('administrator/login', 'refresh');
    	}
    }
		
	

	/*============================================
				End Function Location Controller
	==============================================*/

	
    /*============================================
				Start Function AngularJS
	==============================================*/
	 public function input_data_country()
	{
				$this->load->model('administrator/location/location_model');

				$postdata = file_get_contents("php://input"); // Get Json Data
				if ($postdata){
			    $request = json_decode($postdata); // Decode Json Data
				
				//Main Data
			    $name = $this->security->xss_clean($request->name);
			    $status = $this->security->xss_clean($request->status);

			    $action=$this->location_model->create_new_country($name,$status);
				    if($action){
				    	echo $result = '{"status" : "success","message" : "New country has been succesfully created!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to create a new country!! Please try again later!!"}';
				    }
				} else {
		    		show_404('page');
		    	}
	}
	
	 public function update_data_country()
	{
				$this->load->model('administrator/location/location_model');

				$postdata = file_get_contents("php://input"); // Get Json Data
				if ($postdata){
			    $request = json_decode($postdata); // Decode Json Data
				
				//Main Data
				$pid = $this->security->xss_clean($request->id_country);
				$name = $this->security->xss_clean($request->name);
			    $status = $this->security->xss_clean($request->status);

			    $action=$this->location_model->update_set_country($pid,$name,$status);
				    if($action){
				    	echo $result = '{"status" : "success","message" : "Country has been succesfully updated!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to update country!! Please try again later!!"}';
				    }
				} else {
		    		show_404('page');
		    	}
	}
	
    public function delete_permanent_country()
    {
			$id=$this->session->userdata('admin_id');
			$pid=$this->uri->segment('4');
			if (isset($id) && isset($pid))
			{
				$this->load->model('administrator/location/location_model');
				$action=$this->location_model->delete_permanent_country($pid);
					if($action){
				    	echo $result = '{"status" : "success","message" : "Country has been succesfully deleted!"}';
				    } else {
				    	echo $result = '{"status" : "failure","message" : "Failed to delete country!! Please try again later!!"}';
				    }
			} else {
				show_404('page');
			}
    }
	/*============================================
				End Function AngularJS
	==============================================*/
	
	
	/*============================================
				Start Function View Page
	==============================================*/
	
	//Index Page
	 public function _index_page(){
    	 // Some example data
		$data['title']=$this->page_name;
		$data['heading']="List Country ".$this->page_name;
		$data['page_desc']="List Country ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/location.js');
		$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
        // Load the template from the views directory
        $this->load->model('administrator/location/location_model');
		$getData=$this->location_model->getCountryList();
		$this->smarty->assign('items', $getData);
		$data['content'] = "menu/location/country/index.html";
        $this->parser->parse("layout/main.html",$data);
    }
	 //Index State Page
	 public function _list_state_page($country_id){
    	 // Some example data
		$data['title']=$this->page_name;
		$data['heading']="List State ".$this->page_name;
		$data['page_desc']="List State ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/location.js');
		$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
        // Load the template from the views directory
        $this->load->model('administrator/location/location_model');
		$getData=$this->location_model->getStateList($country_id);
		$this->smarty->assign('items', $getData);
		$data['content'] = "menu/location/state/index.html";
        $this->parser->parse("layout/main.html",$data);
    }

	//Add Page
	public function _add_country_page(){
    	 // Some example data
		$data['title']="Create New Country ".$this->page_name;
		$data['heading']="Create New Country ".$this->page_name;
		$data['page_desc']="Form New Country ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/location.js');
		//$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		//$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
		
        // Load the template from the views directory
        $this->load->model('administrator/location/location_model');
		//$getGenre=$this->location_model->getGenreList();
		//$this->smarty->assign('genrelist', $getGenre);
		$data['content'] = "menu/location/country/add.html";
        $this->parser->parse("layout/main.html",$data);
    }
	
	public function _edit_country_page($country_id){
    	 // Some example data
		$data['title']="Edit Data Country ".$this->page_name;
		$data['heading']="Edit Data Country ".$this->page_name;
		$data['page_desc']="Form Edit Data Country ".$this->page_name;
		
        // for load external js
		$loadJSFiles = array();
		$loadJSFiles[] = base_url('public/js/admin/proui/location.js');
		//$loadJSFiles[] = base_url('themes/proui/js/pages/tablesDatatables.js');
		$this->smarty->assign('loadJSFiles', $loadJSFiles);
		
		 // for init external js
		$initJSFiles = array();
		//$initJSFiles[] = "TablesDatatables.init();";
		$this->smarty->assign('initJSFiles', $initJSFiles);
		
        // Load the template from the views directory
        $this->load->model('administrator/location/location_model');
		$this->smarty->assign('country_id', $country_id);
		$data['content'] = "menu/location/country/edit.html";
        $this->parser->parse("layout/main.html",$data);
    }

	
	/*============================================
				End Function View Page
	==============================================*/

}
