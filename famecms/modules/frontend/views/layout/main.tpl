<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">
<!--<![endif]--><head>

<!-- Basic Page Needs -->
<meta charset="utf-8">
<title>{$title}</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons-->
<link rel="shortcut icon" href="{base_url('themes/edu/img/favicon.ico')}" type="image/x-icon"/>
<link rel="apple-touch-icon" type="image/x-icon" href="{base_url('themes/edu/img/apple-touch-icon-57x57-precomposed.png')}">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{base_url('themes/edu/img/apple-touch-icon-72x72-precomposed.png')}">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{base_url('themes/edu/img/apple-touch-icon-114x114-precomposed.png')}">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{base_url('themes/edu/img/apple-touch-icon-144x144-precomposed.png')}">

<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS -->
<link href="{base_url('themes/edu/css/bootstrap.min.css')}"rel="stylesheet">
<link href="{base_url('themes/edu/css/megamenu.css')}" rel="stylesheet">
<link href="{base_url('themes/edu/css/style.css')}" rel="stylesheet">
<link href="{base_url('themes/edu/font-awesome/css/font-awesome.css')}" rel="stylesheet" >
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<link href="{base_url('themes/edu/css/bootstrap-datetimepicker.min.css')}" rel="stylesheet">
<link rel="stylesheet" href="{base_url('themes/edu/js/fancybox/source/jquery.fancybox.css?v=2.1.4')}">

<!-- REVOLUTION BANNER CSS SETTINGS -->
<link rel="stylesheet" href="{base_url('themes/edu/css/fullwidth.css')}" media="screen" >
<link rel="stylesheet" href="{base_url('themes/edu/rs-plugin/css/settings.css')}" media="screen" >

<style>
textarea,select, input[type="text"], input[type="password"], input[type="datetime"], 
input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], 
input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], 
input[type="tel"], input[type="color"], .uneditable-input {
margin-bottom:5px;
}
.FormStepHeader{
text-align:center;
background-color:blue;
color:#FFF;
padding:10px 0px;
}
</style>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Jquery -->
<script src="{base_url('themes/edu/js/jquery.js')}"></script>
<!-- Support media queries for IE8 -->
<script src="{base_url('themes/edu/js/respond.min.js')}"></script>

<!-- HTML5 and CSS3-in older browsers-->
<script src="{base_url('themes/edu/js/modernizr.custom.17475.js')}"></script>

<!--[if IE 7]>
  <link rel="stylesheet" href="{base_url('themes/edu/font-awesome/css/font-awesome-ie7.min.css')}">
<![endif]-->

</head>

<body ng-app="MainApp">
<div ng-controller="MainController">
{include file="partials/header.tpl"}

{include file="$content"}
    
  
{include file="partials/footer.tpl"}
</div>
<div id="toTop">Back to Top</div>

<!-- DATEPICKER --> 
<script>
		var baseUrl = '{base_url()}';
</script>       
<script type="text/javascript" src="{base_url('themes/edu/js/bootstrap-datetimepicker.min.js')}"></script>
<script type="text/javascript">
      $('#datetimepicker').datetimepicker({
        format: 'dd/MM/yyyy',
		pick12HourFormat: false,   // enables the 12-hour format time picker
  		pickSeconds: false, 
        language: 'en'
      });
</script>
<!-- MEGAMENU --> 
<script src="{base_url('themes/edu/js/jquery.easing.js')}"></script>
<script src="{base_url('themes/edu/js/megamenu.js')}"></script>

<!-- OTHER JS -->    
<script src="{base_url('themes/edu/js/bootstrap.js')}"></script>
<script src="{base_url('themes/edu/js/functions.js')}"></script>
<script src="{base_url('themes/edu/assets/validate.js')}"></script>
<script src="{base_url('node_modules/angular/angular.min.js')}"></script> 
<script src="{base_url('bower_components/angular-bootstrap/ui-bootstrap-tpls-1.1.2.js')}"></script> 
<script src="{base_url('public/js/frontend/edu/controllers/general.js')}"></script>
<script src="{base_url('public/js/frontend/edu/controllers/forms.js')}"></script>

<!-- FANCYBOX -->
<script  src="{base_url('themes/edu/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.4')}" type="text/javascript"></script> 
<script src="{base_url('themes/edu/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5')}" type="text/javascript"></script> 
<script src="{base_url('themes/edu/js/fancy_func.js')}" type="text/javascript"></script> 

 <!-- REVOLUTION SLIDER -->
 <script src="{base_url('themes/edu/rs-plugin/js/jquery.themepunch.plugins.min.js')}"></script>
 <script type="text/javascript" src="{base_url('themes/edu/rs-plugin/js/jquery.themepunch.revolution.min.js')}"></script>
 <script src="{base_url('themes/edu/js/revolutio-slider-func.js')}"></script>

</body>
</html>