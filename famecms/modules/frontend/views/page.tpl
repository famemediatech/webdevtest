<div class="container">

<div class="row">
	<div class="col-md-12">
	<h1>{$title}</h1>
		<ul class="breadcrumb">
			<li><a href="{base_url('page')}/{$slug}">Home</a></li>
			<li class="active">{$title}</li>
		</ul>
	</div>
	{if $ContentPage[0].sidebar_type eq 'sidebar-left' } 
	<!-- =========================Start Col left section ============================= -->   
	<aside class="col-md-4 col-sm-4">
	{$GetSidebar[0].widget_content}
	</aside>
	{/if} 

<!-- =========================Start Col Content section ============================= --> 
	{if $ContentPage[0].sidebar_type eq 'no-sidebar' }
	<section class="col-md-12 col-sm-12">
	{else}
	<section class="col-md-8 col-sm-8">
	{/if}

	<div class="col-right">
    {$ContentPage[0].post_content}
	</div><!-- end col right-->
	</section>
	{if $ContentPage[0].sidebar_type eq 'sidebar-right' }
	<!-- =========================Start Col Right section ============================= -->   
	<aside class="col-md-4 col-sm-4">
	{$GetSidebar[0].widget_content}
	</aside>
	{/if} 
	</div><!-- end row-->
</div> <!-- end container-->