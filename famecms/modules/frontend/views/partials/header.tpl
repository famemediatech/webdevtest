<header>
	<div class="container">
   	  <div class="row">
    	<div class="col-md-4 col-sm-4" id="logo"><a href="index.html"><img src="img/logo.png" alt="Logo"></a></div>
        <div class="col-md-8 col-sm-8">
        
        	<div id="phone" class="hidden-xs"><strong>+44 (0) 123 456 789 </strong>Admissions department</div>
            
            <div id="menu-top">
            	<ul>
                	<li><a href="index.html" title="Home">Home</a> | </li>
                    <li><a href="news-events.html" title="News and Events">News &amp; Events</a> | </li>
                    <li><a href="contact.html" title="Contact">Contact</a></li>
                </ul>
            </div>

        </div><!-- End col-md-8-->
        </div><!-- End row-->
    </div><!-- End container-->
</header><!-- End Header-->

 <nav>
<div class="megamenu_container">
<a id="megamenu-button-mobile" href="#">Menu</a><!-- Menu button responsive-->
    
	<!-- Begin Mega Menu Container -->
	<ul class="megamenu">
		<li><a href="{base_url()}" class="nodrop-down">Home</a></li>
        
        <li class="drop-normal"><a href="javascript:void(0)" class="drop-down">About</a>
        <div class="drop-down-container normal">
                   <ul>
                       <li><a href="{base_url('page/about-rotary')}" title="About Rotary">About Rotary</a></li>
                       <li><a href="{base_url('page/about-rye')}" title="About RYE">About RYE</a></li>
                       <li><a href="{base_url('news')}" title="News">News</a></li>
                    </ul>
         </div><!-- End dropdown normal -->
        </li>
		
		<li><a href="{base_url('page/want-to-participate')}" class="nodrop-down">Want To Participate?</a></li>
		
		<li class="drop-normal"><a href="javascript:void(0)" class="drop-down">Exchange Students</a>
        <div class="drop-down-container normal">
                   <ul>
                       <li><a href="{base_url('page/exchange-student-inbound')}" title="Inbound">Inbound</a></li>
                       <li><a href="{base_url('page/exchange-student-outbound')}" title="Outbound">Outbound</a></li>
                    </ul>
         </div><!-- End dropdown normal -->
        </li>
		
		<li class="drop-normal"><a href="javascript:void(0)" class="drop-down">Forms</a>
        <div class="drop-down-container normal">
                   <ul>
                       <li><a href="{base_url('form/host_family')}" title="Host Family Forms">Host Family Forms</a></li>
                       <li><a href="{base_url('form/inbound_laporan_bulanan')}" title="Inbound Forms">Inbound Forms</a></li>
                       <li><a href="{base_url('forms/outbound-forms')}" title="Outbound Forms">Outbound Forms</a></li>
                       <li><a href="{base_url('forms/counselor-forms')}" title="Counselor Forms">Counselor Forms</a></li>
                    </ul>
         </div><!-- End dropdown normal -->
        </li>

        
	</ul><!-- End Mega Menu -->
</div>
</nav><!-- /navbar -->