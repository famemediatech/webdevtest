<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Mini-Developer version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.0
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Page_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('phpass');

    }
    /* Check */
    public function checkPage($page_name){
    	$this->db->where('post_status', 'publish');
		$this->db->where('post_type', 'page');
		$this->db->where('post_url', $page_name);
		$query=$this->db->get("fame_post");
        if ($query->num_rows() > 0)
        {
        	return true;
        } else {
        	return false;
        }

    }
	 public function getInfoPage($slug)
		{	
			$this->db->where('post_url', $slug);
			$this->db->where('post_type', 'page');
			//$this->db->limit(1);
			$query=$this->db->get("fame_post");
            $result = $query->result_array();
			return $result;
		}
	/* Content */
	 public function getCountry()
		{	
			$this->db->where('is_visible','0');
			$this->db->where('location_type','0');
			$this->db->order_by('name');
			$query=$this->db->get("location");
            $result = $query->result_array();
			return $result;
		}
	public function getContentPage($page_name)
		{	
			$this->db->where('post_status', 'publish');
			$this->db->where('post_type', 'page');
			$this->db->where('post_url', $page_name);
			//$this->db->limit(1);
			$query=$this->db->get("fame_post");
            $result = $query->result_array();
			return $result;
		}
	public function getSidebar($id)
		{	
			$this->db->where('id_widget', $id);
			//$this->db->limit(1);
			$query=$this->db->get("fame_widget");
            $result = $query->result_array();
			return $result;
		}
	}

	
