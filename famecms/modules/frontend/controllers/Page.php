<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * FameCMS
 *
 * Codeigniter CMS + Angular
 *
 * @package   FameCMS (Mini-Developer version)
 * @author    Faizal Amry (Famry)
 * @copyright 2015 Faizal Amry
 * @link      http:/famecms.com
 * @license   MIT
 * @version   1.0
 ==============================================================
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 
 */

class Page extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        $this->load->library('parser');
        $this->load->helper('frontend');
        //$this->load->helper('phpass');
    }

   public function index($page_name="")
    {
        $this->load->model('frontend/getdata/page_model');
        if ($page_name){
            $check_page = $this->page_model->checkPage($page_name);
                if($check_page)
                {
					$detail_page= $this->page_model->getInfoPage($page_name);
                    $this->_show_page($detail_page);
                } else {
                    show_404('page');
                }
               
        } else {
                show_404('page');
        }
       
    }
	

    public function _show_page($detail_page){
        $data['title']=$detail_page[0]['post_title'];
        $data['slug']=$detail_page[0]['post_url'];
		
        $this->load->model('frontend/getdata/page_model');

        $getContentPage=$this->page_model->getContentPage($detail_page[0]['post_url']);
        $getSidebar=$this->page_model->getSidebar($detail_page[0]['sidebar_id']);



        $this->smarty->assign('ContentPage', $getContentPage);
        $this->smarty->assign('GetSidebar', $getSidebar);

        $data['content'] = "page.tpl";
        $this->parser->parse("layout/main.tpl",$data);
    }    

}
