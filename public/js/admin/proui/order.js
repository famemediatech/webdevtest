fameAdminApp.controller('ManageOrderCtrl', function($scope,$http,$window) {
	//Action Delete Permanent
	$scope.deletePermanentAction = function(id){
		if($window.confirm("This Menu will be delete permanently and can't be restored.Continue??")) {
		waitingDialog.show('Please Wait...');
	        $http({
		    method: 'POST',
		    url: baseUrl+'administrator/menu/delete_permanent_data/'+id,
		    headers: {'Content-Type': 'application/json'},
		    })
		    .success(function (data , status, headers, cfg) {
		    if(data.status == 'success'){
				waitingDialog.show(data.message);
				window.location.href= baseUrl+"administrator/menu";
		    }else{
				waitingDialog.hide();
				alert(data.message);
		    }
		    })
		    .error(function(data, status, headers, cfg) {
				waitingDialog.hide();
		        alert('There was a network error. Try again later.');
		       });
	      } else {
	      	alert('Deleting permanent menu has been canceled!!');
	      }
	};
 });

fameAdminApp.controller('AddOrderCtrl', function($scope,$http) {
  $scope.dataForm = {};
  $scope.dataMenu = {};
  $scope.activeMenu = []
	// Init default dropdown value
  $scope.buttonAddMenu = function(newData){
    var splitMenu = newData.menuList.split(' || ');
    $scope.activeMenu.push({id: splitMenu[1],name: splitMenu[0],qty: newData.qty,price: splitMenu[2]})
    $scope.dataMenu = {};
  };
  $scope.buttonDeleteItem = function(index){
    $scope.activeMenu.splice(index, 1);
  };
	$scope.buttonAdd = function(newData){
  newData.activeMenu = $scope.activeMenu;
	waitingDialog.show('Please Wait...');

	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/order/input_data',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(newData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/order";
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
    	};
 });
  
 fameAdminApp.controller('EditOrderCtrl', function($scope,$http) {
  $scope.dataForm = {};
  $scope.dataMenu = {};
	// Init TinyMCE Options
	
	angular.element(document).ready(function () {
	waitingDialog.show('Please Wait...');
      $scope.get_old_data = $http.get(baseUrl+'administrator/order/getOldData/'+$scope.id_order).
		success(function(data) {
				waitingDialog.hide();
				$scope.dataForm = data[0];
				$scope.activeMenu = JSON.parse(data[0].order_detail);
		})
		.error(function(data, status) {
			waitingDialog.hide();
			alert("Error !! Please try again later!!");
		});
    });
  $scope.buttonAddMenu = function(newData){
    var splitMenu = newData.menuList.split(' || ');
    $scope.activeMenu.push({id: splitMenu[1],name: splitMenu[0],qty: newData.qty,price: splitMenu[2]})
    $scope.dataMenu = {};
  };
  $scope.buttonDeleteItem = function(index){
    $scope.activeMenu.splice(index, 1);
  };
	$scope.buttonUpdate = function(oldData){
  //console.log(oldData);
  oldData.activeMenu = $scope.activeMenu;
	waitingDialog.show('Please Wait...');
	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/order/update_data',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(oldData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/order/edit/"+oldData.order_id+'?update=success';
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
     };
});
fameAdminApp.controller('PaymentOrderCtrl', function($scope,$http) {
  $scope.dataForm = {};
  $scope.dataMenu = {};
  $scope.vat = 0.1;
	// Init TinyMCE Options
	
	angular.element(document).ready(function () {
	waitingDialog.show('Please Wait...');
      $scope.get_old_data = $http.get(baseUrl+'administrator/order/getOldData/'+$scope.id_order).
		success(function(data) {
				waitingDialog.hide();
				$scope.dataForm = data[0];
        $scope.activeMenu = JSON.parse(data[0].order_detail);
        $scope.activeMenu.forEach(function(entry) {
          $scope.dataForm.subTotal = $scope.dataForm.subTotal ? $scope.dataForm.subTotal : 0 + (entry.price * entry.qty);
        });
        $scope.dataForm.payment_detail = JSON.parse(data[0].payment_detail);
        $scope.dataForm.vatPercent = $scope.vat * 100;
        $scope.dataForm.vatDue = $scope.dataForm.subTotal * $scope.vat;
        $scope.dataForm.totalDue = $scope.dataForm.subTotal +  $scope.dataForm.vatDue;
		})
		.error(function(data, status) {
			waitingDialog.hide();
			alert("Error !! Please try again later!!");
		});
    });
  $scope.buttonPay = function(oldData){
   if(oldData.cashPay < oldData.totalDue){
     alert('Cash must be greater than transaction bill');
     return false;
   }
   var paymentDetail = [{
    cashPay: oldData.cashPay,
    transID: oldData.trans_id,
    subTotal: oldData.subTotal,
    vatDue: oldData.vatDue,
    vatPercent:oldData.vatPercent
   }];
   oldData.paymentDetail = paymentDetail;
   waitingDialog.show('Please Wait...');
	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/order/payment_data',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(oldData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/order/payment/"+oldData.order_id+'?payment=success';
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
      alert('There was a network error. Try again later.');
      });
  };
	$scope.buttonUpdate = function(oldData){
  //console.log(oldData);
  oldData.activeMenu = $scope.activeMenu;
	waitingDialog.show('Please Wait...');
	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/order/update_data',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(oldData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/order/edit/"+oldData.order_id+'?update=success';
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
     };
  })