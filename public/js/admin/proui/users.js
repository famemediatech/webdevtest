fameAdminApp.controller('ManageUserCtrl', function ($scope) {

});
fameAdminApp.controller('AddUserCtrl', function ($scope,$http) {
$scope.dataForm = {};
$scope.dataForm.role = '2';
$scope.dataForm.status = 'active';
$scope.buttonAdd = function(newData){
	waitingDialog.show('Please Wait...');
	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/users/input_data',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(newData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/users";
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
   	};
});

// Directive
// Email Exist
	fameAdminApp.directive('emailAvailable', ['$http', function($http) {
	  return {
	    require : 'ngModel',
	    link : function(scope, element, attrs, ngModel) {
	      ngModel.$asyncValidators.emailAvailable = function(email) {
	        return $http.post(baseUrl+'administrator/checkExistEmail', {email:email}).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		    if(data.status == 'success'){
		    ngModel.$setValidity("unique", true);
		    } else {
		    ngModel.$setValidity("unique", false);
		    }
		    
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    alert('There was a network error. Try again later.');
		  });
	      };
	    }
	  }
	}])
	// Username Exist
	fameAdminApp.directive('usernameAvailable', ['$http', function($http) {
	  return {
	    require : 'ngModel',
	    link : function(scope, element, attrs, ngModel) {
	      ngModel.$asyncValidators.usernameAvailable = function(username) {
	        return $http.post(baseUrl+'administrator/checkExistUsername', {username:username}).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		    if(data.status == 'success'){
		    ngModel.$setValidity("unique", true);
		    } else {
		    ngModel.$setValidity("unique", false);
		    }
		    
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    alert('There was a network error. Try again later.');
		  });
	      };
	    }
	  }
	}])
	 // Password Match
	 fameAdminApp.directive('passwordMatch', [function () {
	    return {
	        restrict: 'A',
	        scope:true,
	        require: 'ngModel',
	        link: function (scope, elem , attrs,control) {
	            var checker = function () {
	 
	                //get the value of the first password
	                var e1 = scope.$eval(attrs.ngModel); 
	 
	                //get the value of the other password  
	                var e2 = scope.$eval(attrs.passwordMatch);
	                return e1 == e2;
	            };
	            scope.$watch(checker, function (n) {
	 
	                //set the form control to valid if both 
	                //passwords are the same, else invalid
	                control.$setValidity("unique", n);
	            });
	        }
	    };
	}]);