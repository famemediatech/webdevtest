 fameAdminApp.controller('ManageCategoryCtrl', function($scope,$http,$window) {
	//Action Delete Permanent
	$scope.deletePermanentAction = function(id){
		if($window.confirm("This Category will be delete permanently and can't be restored.Continue??")) {
		waitingDialog.show('Please Wait...');
	        $http({
		    method: 'POST',
		    url: baseUrl+'administrator/category/delete_permanent_data/'+id,
		    headers: {'Content-Type': 'application/json'},
		    })
		    .success(function (data , status, headers, cfg) {
		    if(data.status == 'success'){
				waitingDialog.show(data.message);
				window.location.href= baseUrl+"administrator/category";
		    }else{
				waitingDialog.hide();
				alert(data.message);
		    }
		    })
		    .error(function(data, status, headers, cfg) {
				waitingDialog.hide();
		        alert('There was a network error. Try again later.');
		       });
	      } else {
	      	alert('Deleting permanent category has been canceled!!');
	      }
	};
 });

fameAdminApp.controller('AddCategoryCtrl', function($scope,$http) {
	$scope.dataForm = {};
	
	// Init default dropdown value
	$scope.dataForm.status = 'active';
	$scope.dataForm.type = 'post';
	$scope.dataForm.parent = '0';

	$scope.buttonAdd = function(newData){
	waitingDialog.show('Please Wait...');

	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/category/input_data',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(newData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/category";
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
   	};
 });
  
 fameAdminApp.controller('EditCategoryCtrl', function($scope,$http) {
	$scope.dataForm = {}; 
	// Init TinyMCE Options
	
	angular.element(document).ready(function () {
	waitingDialog.show('Please Wait...');
      $scope.get_old_data = $http.get(baseUrl+'administrator/category/getOldData/'+$scope.id_category).
		success(function(data) {
				waitingDialog.hide();
				$scope.dataForm = data[0];
				
		})
		.error(function(data, status) {
			waitingDialog.hide();
			alert("Error !! Please try again later!!");
		});
    });
	
	$scope.buttonUpdate = function(oldData){
	//console.log(oldData);
	waitingDialog.show('Please Wait...');
	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/category/update_data',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(oldData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/category/edit/"+oldData.id_category+'?update=success';
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
   	};
});