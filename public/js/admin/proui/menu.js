fameAdminApp.controller('ManageMenuCtrl', function($scope,$http,$window) {
	//Action Delete Permanent
	$scope.deletePermanentAction = function(id){
		if($window.confirm("This Menu will be delete permanently and can't be restored.Continue??")) {
		waitingDialog.show('Please Wait...');
	        $http({
		    method: 'POST',
		    url: baseUrl+'administrator/menu/delete_permanent_data/'+id,
		    headers: {'Content-Type': 'application/json'},
		    })
		    .success(function (data , status, headers, cfg) {
		    if(data.status == 'success'){
				waitingDialog.show(data.message);
				window.location.href= baseUrl+"administrator/menu";
		    }else{
				waitingDialog.hide();
				alert(data.message);
		    }
		    })
		    .error(function(data, status, headers, cfg) {
				waitingDialog.hide();
		        alert('There was a network error. Try again later.');
		       });
	      } else {
	      	alert('Deleting permanent menu has been canceled!!');
	      }
	};
 });

fameAdminApp.controller('AddMenuCtrl', function($scope,$http) {
	$scope.dataForm = {};
	
	// Init default dropdown value
	$scope.dataForm.status = '0';
	$scope.dataForm.category = '1';
	$scope.dataForm.price = '0';

	$scope.buttonAdd = function(newData){
	waitingDialog.show('Please Wait...');

	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/menu/input_data',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(newData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/menu";
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
   	};
 });
  
 fameAdminApp.controller('EditMenuCtrl', function($scope,$http) {
	$scope.dataForm = {}; 
	// Init TinyMCE Options
	
	angular.element(document).ready(function () {
	waitingDialog.show('Please Wait...');
      $scope.get_old_data = $http.get(baseUrl+'administrator/menu/getOldData/'+$scope.id_menu).
		success(function(data) {
				waitingDialog.hide();
				$scope.dataForm = data[0];
				
		})
		.error(function(data, status) {
			waitingDialog.hide();
			alert("Error !! Please try again later!!");
		});
    });
	
	$scope.buttonUpdate = function(oldData){
	//console.log(oldData);
	waitingDialog.show('Please Wait...');
	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/menu/update_data',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(oldData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/menu/edit/"+oldData.id_menu+'?update=success';
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
   	};
});