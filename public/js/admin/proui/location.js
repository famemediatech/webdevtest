 fameAdminApp.controller('ManageCountryCtrl', function($scope,$http,$window) {
	//Action Delete Permanent
	$scope.deletePermanentAction = function(id){
		if($window.confirm("This country will be delete permanently and can't be restored.Continue??")) {
		waitingDialog.show('Please Wait...');
	        $http({
		    method: 'POST',
		    url: baseUrl+'administrator/location/delete_permanent_country/'+id,
		    headers: {'Content-Type': 'application/json'},
		    })
		    .success(function (data , status, headers, cfg) {
		    if(data.status == 'success'){
				waitingDialog.show(data.message);
				window.location.href= baseUrl+"administrator/location";
		    }else{
				waitingDialog.hide();
				alert(data.message);
		    }
		    })
		    .error(function(data, status, headers, cfg) {
				waitingDialog.hide();
		        alert('There was a network error. Try again later.');
		       });
	      } else {
	      	alert('Deleting permanent country has been canceled!!');
	      }
	};
 });
  fameAdminApp.controller('ManageStateCtrl', function($scope,$http,$window) {
	//Action Delete Permanent
	$scope.deletePermanentAction = function(id){
		if($window.confirm("This state will be delete permanently and can't be restored.Continue??")) {
		waitingDialog.show('Please Wait...');
	        $http({
		    method: 'POST',
		    url: baseUrl+'administrator/location/delete_permanent_states/'+id,
		    headers: {'Content-Type': 'application/json'},
		    })
		    .success(function (data , status, headers, cfg) {
		    if(data.status == 'success'){
				waitingDialog.show(data.message);
				window.location.href= baseUrl+"administrator/location";
		    }else{
				waitingDialog.hide();
				alert(data.message);
		    }
		    })
		    .error(function(data, status, headers, cfg) {
				waitingDialog.hide();
		        alert('There was a network error. Try again later.');
		       });
	      } else {
	      	alert('Deleting permanent state has been canceled!!');
	      }
	};
 });

  fameAdminApp.controller('AddCountryCtrl', function($scope,$http) {
	$scope.dataForm = {};
	
	// Init default dropdown value
	$scope.dataForm.status = '0';
	
	$scope.buttonAdd = function(newData){
	waitingDialog.show('Please Wait...');
	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/location/input_data_country',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(newData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/location";
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
   	};
 });

 fameAdminApp.controller('EditCountryCtrl', function($scope,$http) {
	$scope.dataForm = {}; 
	// Init TinyMCE Options
	
	angular.element(document).ready(function () {
	waitingDialog.show('Please Wait...');
      $scope.get_old_data = $http.get(baseUrl+'administrator/location/getOldDataCountry/'+$scope.id_country).
		success(function(data) {
				waitingDialog.hide();
				$scope.dataForm = data[0];
				//$scope.dataForm.publish_date = moment(data[0].publish_date).format('MMMM Do YYYY');
				
		})
		.error(function(data, status) {
			waitingDialog.hide();
			alert("Error !! Please try again later!!");
		});
    });
	
	$scope.buttonUpdate = function(oldData){
	//console.log(oldData);
	waitingDialog.show('Please Wait...');
	    $http({
	    method: 'POST',
	    url: baseUrl+'administrator/location/update_data_country',
	    headers: {'Content-Type': 'application/json'},
	    data: JSON.stringify(oldData),
	    })
	    .success(function (data , status, headers, cfg) {
	    if(data.status == 'success'){
	       waitingDialog.show(data.message);
	       window.location.href= baseUrl+"administrator/location/edit_country/"+oldData.id_country+'?update=success';
	    }else{
		  waitingDialog.hide();
	      alert(data.message);
	    }
	    })
	    .error(function(data, status, headers, cfg) {
			waitingDialog.hide();
	        alert('There was a network error. Try again later.');
	       });
   	};
});